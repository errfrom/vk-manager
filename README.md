# vk-manager

Средство, предназначенное для систематической  
выгрузки общедоступной информации, размещенной  
в социальной сети 'ВКонтакте', а также для её  
последующей структуризации с целью представления  
в удобном для анализа виде.  

## Установка и запуск

Последнюю версию vk-manager можно получить, воспользовавшись прямой ссылкой: [vkmanager-0.2.0](https://bitbucket.org/errfrom/vk-manager/downloads/vkmanager-0.2.0.zip).

Далее следует распаковать архив и выполнить 'run' скрипт с требуемыми вами параметрами.  
Для получения справки по использованию, достаточно запустить скрипт с параметром '-h'.  

## Лицензия

*Copyright © 2017 Ivanov Dmitry (errfrom)  
vk-manager is distributed under the terms of the MIT license.*